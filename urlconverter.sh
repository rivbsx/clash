#!/data/data/com.termux/files/usr/bin/bash

backend='https://sub.xeton.dev'
rule='https://codeberg.org/rivbsx/clash/raw/branch/main/rule.ini'
subscription="${1}"
len_sub=${#subscription}
encoded_sub=""
len_rule=${#rule}
encoded_rule=""

# encode subscription
for (( pos=0 ; pos<len_sub ; pos++ )); do
    c=${subscription:$pos:1}
    case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02X' "'$c"
    esac
    encoded_sub+="${o}"
done

# encode rule
for (( pos=0 ; pos<len_rule ; pos++ )); do
    c=${rule:$pos:1}
    case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02X' "'$c"
    esac
    encoded_rule+="${o}"
done

echo
echo "${backend}/sub?target=clash&url=${encoded_sub}&config=${encoded_rule}"
